#   Interact with Sharesight APIs
#   ≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
# 
#   This script calls APIs on Sharesight.
# 
#   It uses a user's API credentials.

if splitdir(pwd())[2] == "notebook"
    @info "Running from the notebook subdirectory, need to run from the root folder, will cd to there."
    cd("..") # Switch to the project root folder
end
println("Running from: $(pwd())")

# Dependencies
#include("./src/Sharesight.jl")

using Sharesight

using HTTP
using JSON
using JSONTables
using DataFrames
#using CSV
using Dates

# Set up the secrets to login

using ArgParse

function parse_commandline()
    s = ArgParseSettings(description="""Specify the credentials for accessing InvestNow. 
    No arguments are mandatory. 
    Each of username, password and managerId are taken from the following sources, listed in decreasing preference: command line arguments, environment variable, configuration file.

    
    If no arguments are secified and the environemnt variables are not all specified then the default configuration file "./config/secrets.jl" will be used.

    The configuration file should be executable julia code and should set the variables username, password and managerId.
    """)

# If no arguments are specified the following environment variables will be used (if they all exist): INVESTNOW_USERNAME, INVESTNOW_PASSWORD and INVESTNOW_MANAGERID.

    @add_arg_table! s begin
        "--outdir", "-o"
            help = "The directory used for the output files."
            arg_type = String
            default = "./output/"
        "--secretsfile", "-s"
            help = "The file containing the credentials for logging in to InvestNow"
            default = "./config/secrets.jl"
        "--client_id"
            help = "Sharesight API client_id. Overrides the SHARESIGHT_CLIENT_ID environment variable if it has been set."
        "--client_secret"
            help = "Sharesight API client_secret. Overrides the SHARESIGHT_CLIENT_SECRET environment variable if it has been set."
        "--exitbeforrunning"
            help = "specify this to check that the arguments have been set but don't actually call InvestNow"
            action = :store_true
    end

    return parse_args(s)
end

#println(pwd())

parsed_args = parse_commandline()


# NOTE: To enable debug level log output, set the environment variable JULIA_DEBUG=SharesightRunner 
#      or run using the command line:
#         $ JULIA_DEBUG=SharesightRunner julia --project=. SharesightRunner.jl
@debug "Command line arguments: $(parsed_args)"

client_id = nothing
client_secret = nothing

# Start with configuration files:
if parsed_args["secretsfile"]  !== nothing
    # Non-default configuration file
    @debug "Using secrets file: $(parsed_args["secretsfile"])"

    if isfile(parsed_args["secretsfile"])
        include(parsed_args["secretsfile"])
        credentials = SharesightClientCredentials(client_id, client_secret)
    else
        @info("The secrets (config) file does not exist and has not been used: $(parsed_args["secretsfile"])")
    end
end

# Override with command line arguments or environment variables
if parsed_args["client_id"] !== nothing
    @debug "client_id from command line"
    client_id = parsed_args["client_id"]
elseif haskey(ENV, "SHARESIGHT_CLIENT_ID")
    @debug "client_id from environment variable"
    client_id = ENV["SHARESIGHT_CLIENT_ID"]
end

if parsed_args["client_secret"] !== nothing
    @debug "client_secret from command line"
    client_secret = parsed_args["client_secret"]
elseif haskey(ENV, "SHARESIGHT_CLIENT_SECRET")
    @debug "client_secret from environment variable"
    client_secret = ENV["SHARESIGHT_CLIENT_SECRET"]
end

if client_id !== nothing && client_secret !== nothing
    credentials = SharesightClientCredentials(client_id, client_secret)
    println("Using client_id starting $(SubString(credentials.client_id, 1, 5)) and client_secret starting $(SubString(credentials.client_secret, 1, 5))")
else
    println("Credentials have not been provided - exiting!")
    if client_id === nothing
        println("client_id has not been provided.")
    end
    if client_secret === nothing
        println("client_secret has not been provided.")
    end
    exit()
end

oauthinfo = nothing

# Get portfolios
oauthinfo, portfolios = Sharesight.get_portfolios(oauthinfo, credentials)

@info portfolios