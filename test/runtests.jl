using Test
using Dates
using Sharesight
using DataFrames

@testset "Check testing works" begin
    @test 1 == 2 - 1
end

@testset "SharesightTransaction constructor" begin
    sstx = SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Buy 340  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.194", 340.0, 9.194, 3125.96, "NZD", "unique01")
    @test sstx.tradedate == Date("2021-07-26")
    @test sstx.transactiontype == "BUY"
    @test sstx.amount == 3125.96
    @test sstx.marketcode == "NZX"
    @test sstx.instrumentcode == "MZY"
    @test sstx.comment == "Buy 340  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.194"
    @test sstx.quantity == 340.0
    @test sstx.price == 9.194
    @test sstx.amount == 3125.96
    @test sstx.currency === "NZD"
    @test sstx.uniqueId === "unique01"
end

@testset "SharesightTransaction constructor partially specified" begin
    sstx = SharesightTransaction(Date("2021-07-26"), nothing, nothing, nothing, nothing, nothing, nothing, nothing, nothing)
    @test sstx.tradedate == Date("2021-07-26")
    @test isnothing(sstx.transactiontype)
    @test isnothing(sstx.marketcode)
    @test isnothing(sstx.instrumentcode)
    @test isnothing(sstx.comment)
    @test isnothing(sstx.quantity)
    @test isnothing(sstx.price)
    @test isnothing(sstx.amount)
    @test isnothing(sstx.currency)
    @test isnothing(sstx.uniqueId)
end

@testset "buysellfilter" begin
    @test Sharesight.buysellfilter(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Buy 340  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.194", 340.0, 9.194, 3125.96, "NZD")) == true
    @test Sharesight.buysellfilter(SharesightTransaction(Date("2021-07-26"), "Sell", "NZX", "MZY", "Buy 340  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.194", 340.0, 9.194, 3125.96, "NZD")) == true
    @test Sharesight.buysellfilter(SharesightTransaction(Date("2021-07-26"), "Reinvest", "NZX", "MZY", "Buy 340  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.194", 340.0, 9.194, 3125.96, "NZD")) == false
    @test Sharesight.buysellfilter(SharesightTransaction(Date("2021-07-26"), nothing, nothing, nothing, nothing, nothing, nothing, nothing, nothing, nothing)) == false
end

@testset "isless" begin
    @test !(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD") < SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD"))
    @test !(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD") > SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD"))
    @test SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD") == SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD")
    @test SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD") === SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD")

    # Amount different due to rounding
    @test !(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 47.0, 9.195, 429.81, "NZD") < SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 47.0, 9.195, 429.82, "NZD"))
    @test !(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 47.0, 9.195, 429.81, "NZD") < SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 47.0, 9.195, 429.82, "NZD"))

    @test SharesightTransaction(Date("2021-07-25"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD") < SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD")
    @test isless(SharesightTransaction(Date("2021-07-25"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD") , SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD"))
    @test SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD") > SharesightTransaction(Date("2021-07-25"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD")

    @test SharesightTransaction(Date("2021-07-26"), "Buy", "NZA", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD") < SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD")
    @test SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZX", "Test", 340.0, 9.194, 3125.96, "NZD") < SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD")
    @test SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZX", "Test", 339.0, 9.194, 3125.96, "NZD") < SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD")
    @test SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZX", "Test", 340.0, 9.193, 3125.96, "NZD") < SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD")
    @test SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZX", "Test", 340.0, 9.194, 3125.95, "NZD") < SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD")

    @test SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD") < SharesightTransaction(Date("2021-07-26"), "Sell", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD")
    @test isless(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD") , SharesightTransaction(Date("2021-07-26"), "Sell", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD"))

    @test SharesightTransaction(Date("2021-07-26"), "Sell", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD") > SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 340.0, 9.194, 3125.96, "NZD")

end

@testset "isequal" begin
    @test isequal(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.194, 3125.96, "NZD") , SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD"))
    @test isequal(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.194, 3125.96, "NZD", "unique01") , SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD", "unique01"))
    @test !isequal(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.194, 3125.96, "NZD", "unique01") , SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD", "unique02"))

    # uniqueId ignored if is nothing
    @test isequal(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.194, 3125.96, "NZD", "unique01") , SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD"))

    @test !isequal(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.194, 3125.96, "NZD") , SharesightTransaction(Date("2021-07-26"), "Sell", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD"))

    # Amount different due to rounding but ignored for equality
    @test isequal(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 47.0, 9.195, 429.81, "NZD") , SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test", 47.0, 9.195, 429.82, "NZD"))

    @test !isequal(SharesightTransaction(Date("2021-07-27"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.194, 3125.96, "NZD") , SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD"))
    @test !isequal(SharesightTransaction(Date("2021-07-26"), "Buy", "NZY", "MZY", "Test 1", 340.0, 9.194, 3125.96, "NZD") , SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD"))
    @test !isequal(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZZ", "Test 1", 340.0, 9.194, 3125.96, "NZD") , SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD"))

    @test !isequal(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 341.0, 9.194, 3125.96, "NZD") , SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD"))

    @test !isequal(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.195, 3125.96, "NZD") , SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD"))

    # amount can be up to 0.01 different and still be equal 
    # amount ignored in equality comparison
    @test isequal(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.194, 3125.97, "NZD") , SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD"))
    @test isequal(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.194, 3125.97, "NZD") , SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.95, "NZD"))
    @test isequal(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.194, 3125.97, "NZD") , SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.98, "NZD"))

    # Use uniqueId for equality
    @test isequal(SharesightTransaction(Date("2022-07-26"), "Sell", "ASX", "NZP", "Test 1", 440.0, 19.194, 13125.97, "AUD", "unique01") , SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD", "unique01"))
end

@testset "==" begin
    @test SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.194, 3125.96, "NZD") == SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD")
    @test SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.194, 3125.96, "NZD", "unique01") == SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD", "unique01")
    @test SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.194, 3125.96, "NZD", "unique01") != SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD", "unique02")

    @test SharesightTransaction(Date("2021-07-27"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.194, 3125.96, "NZD") != SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD")
    @test SharesightTransaction(Date("2021-07-26"), "Buy", "NZY", "MZY", "Test 1", 340.0, 9.194, 3125.96, "NZD") != SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD")
    @test SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZZ", "Test 1", 340.0, 9.194, 3125.96, "NZD") != SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD")

    @test SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 341.0, 9.194, 3125.96, "NZD") != SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD")

    @test SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.195, 3125.96, "NZD") != SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD")

    # amount can be up to 0.01 different
    # amount ignored in equality comparison
    @test SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.194, 3125.97, "NZD") == SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD")
    @test SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.194, 3125.97, "NZD") == SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.95, "NZD")
    @test SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.194, 3125.97, "NZD") == SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.98, "NZD")

    @test SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.194, 3125.96, "NZD") != SharesightTransaction(Date("2021-07-26"), "Sell", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD")

    # Use uniqueId for equality
    @test SharesightTransaction(Date("2022-07-26"), "Sell", "ASX", "NZP", "Test 1", 440.0, 19.194, 13125.97, "AUD", "unique01") == SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD", "unique01")
end

@testset "hash" begin
    @test hash(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.194, 3125.96, "NZD")) == hash(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD"))

    @test hash(SharesightTransaction(Date("2021-07-27"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.194, 3125.96, "NZD")) != hash(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD"))
    @test hash(SharesightTransaction(Date("2021-07-26"), "Buy", "NZY", "MZY", "Test 1", 340.0, 9.194, 3125.96, "NZD")) != hash(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD"))
    @test hash(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZZ", "Test 1", 340.0, 9.194, 3125.96, "NZD")) != hash(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD"))

    @test hash(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 341.0, 9.194, 3125.96, "NZD")) != hash(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD"))

    @test hash(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.195, 3125.96, "NZD")) != hash(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD"))

    # Amount is not used for hash to ignore rounding errors
    @test hash(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.194, 3125.97, "NZD")) == hash(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD"))

    @test hash(SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Test 1", 340.0, 9.194, 3125.96, "NZD")) != hash(SharesightTransaction(Date("2021-07-26"), "Sell", "NZX", "MZY", "Test 2", 340.0, 9.194, 3125.96, "NZD"))
end

@testset "get_uri" begin
    @test Sharesight.get_uri("V3", "getthisuri"; parameters="?end_date=2023-02-17&start_date=2022-02-17") == "https://api.sharesight.com/api/v3/getthisuri?end_date=2023-02-17&start_date=2022-02-17"
    @test Sharesight.get_uri("V2", "getthisuri"; parameters="?end_date=2023-02-17&start_date=2022-02-17") == "https://api.sharesight.com/api/v2/getthisuri?end_date=2023-02-17&start_date=2022-02-17"

    @test Sharesight.get_uri("V3", "getthisuri"; parameters="end_date=2023-02-17&start_date=2022-02-17") == "https://api.sharesight.com/api/v3/getthisuri?end_date=2023-02-17&start_date=2022-02-17"
    @test Sharesight.get_uri("V2", "getthisuri"; parameters="end_date=2023-02-17&start_date=2022-02-17") == "https://api.sharesight.com/api/v2/getthisuri?end_date=2023-02-17&start_date=2022-02-17"

    @test Sharesight.get_uri("V3", "getthisuri") == "https://api.sharesight.com/api/v3/getthisuri"
    @test Sharesight.get_uri("V2", "getthisuri") == "https://api.sharesight.com/api/v2/getthisuri"
end

in_ss_txn_set_df = DataFrame([
    SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Buy 340  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.194", 340.0, 9.194, 3125.96, "NZD"),
    SharesightTransaction(Date("2021-07-25"), "Sell", "NZX", "MZY", "Sell 34  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.194", 34.0, 9.194, 312.596, "NZD"),
    SharesightTransaction(Date("2021-08-26"), "Buy", "NZX", "MZY", "Buy 350  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.21", 350.0, 9.21, 3223.50, "NZD"),
    SharesightTransaction(Date("2021-09-26"), "Buy", "NZX", "MZY", "Buy 360  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.31", 360.0, 9.31, 3351.60, "NZD")
])

ss_ss_txn_set_df = DataFrame([
    SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Buy 340  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.194", 340.0, 9.194, 3125.96, "NZD"),
    SharesightTransaction(Date("2021-07-25"), "Sell", "NZX", "MZY", "Sell 34  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.194", 34.0, 9.194, 312.596, "NZD")
])

@testset "FInd new transactons from InvestNow that Sharesight doesn't know about" begin
    @test nrow(in_ss_txn_set_df) == 4
    @test nrow(ss_ss_txn_set_df) == 2

    # Compare individual columns
    unrecorded_trades_df = antijoin(in_ss_txn_set_df, ss_ss_txn_set_df, on=[:tradedate => :tradedate, :transactiontype => :transactiontype, :marketcode => :marketcode, :instrumentcode => :instrumentcode], makeunique=true)

    @test nrow(unrecorded_trades_df) == 2
end

in_ss_txn_df = DataFrame(txn = [
    SharesightTransaction(Date("2021-08-26"), "Buy", "NZX", "MZY", "Buy 350  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.21", 350.0, 9.21, 3223.50, "NZD", "unique01"),
    SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "Buy 340  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.194", 340.0, 9.194, 3125.96, "NZD", "unique02"),
    SharesightTransaction(Date("2021-09-26"), "Buy", "NZX", "MZY", "Buy 360  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.31", 360.0, 9.31, 3351.60, "NZD", "unique03"),
    SharesightTransaction(Date("2021-09-28"), "Reinvest", "NZX", "MZY", "Reinvest 36  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.31", 36.0, 9.31, 335.16, "NZD", "unique04"),
    SharesightTransaction(Date("2021-07-25"), "Sell", "NZX", "MZY", "Sell 34  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.194", 34.0, 9.194, 312.596, "NZD", "unique05")
])

ss_ss_txn_df = DataFrame(txn = [
    SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "XXXX Buy 340  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.194", 340.0, 9.194, 3125.96, "NZD"),
    SharesightTransaction(Date("2021-07-25"), "Sell", "NZX", "MZY", "XXXX Sell 34  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.194", 34.0, 9.194, 312.596, "NZD")
])

ss_ss_txn_renamed_df = DataFrame(ss_txn = [
    SharesightTransaction(Date("2021-07-26"), "Buy", "NZX", "MZY", "XXXX Buy 340  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.194", 340.0, 9.194, 3125.96, "NZD"),
    SharesightTransaction(Date("2021-07-25"), "Sell", "NZX", "MZY", "XXXX Sell 34  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.194", 34.0, 9.194, 312.596, "NZD")
])

@testset "FInd new transactons structs from InvestNow that Sharesight doesn't know about" begin
    @test nrow(in_ss_txn_df) == 5
    @test nrow(ss_ss_txn_df) == 2

    # Compare transaction structs
    unrecorded_trades_df = antijoin(in_ss_txn_df, ss_ss_txn_df, on=:txn)
    @test nrow(unrecorded_trades_df) == 3

    # Compare transaction structs
    unrecorded_trades_df = antijoin(in_ss_txn_df, ss_ss_txn_df, on=[:txn => :txn])
    @test nrow(unrecorded_trades_df) == 3

    # Compare transaction structs
    unrecorded_trades_df = antijoin(in_ss_txn_df, ss_ss_txn_renamed_df, on=[:txn => :ss_txn])
    @test nrow(unrecorded_trades_df) == 3
end

@testset "buysellfilter on a DataFrame" begin
    @test nrow(filter(:txn => Sharesight.buysellfilter, in_ss_txn_df)) == 4
    @test nrow(filter(:txn => txn -> Sharesight.buysellfilter(txn), in_ss_txn_df)) == 4
end

trades_response = """{"trades":[{"id":70040,"unique_identifier":"InvestNow:2022-02-23:489.9:132413:Buy 89  Smartshares - NZ Mid Cap ETF (MDZ) at 5.504494","transaction_date":"2022-02-23","quantity":89.0,"price":5.504494,"cost_base":null,"exchange_rate":1.0,"brokerage":0.0,"brokerage_currency_code":"NZD","value":489.9,"paid_on":null,"company_event_id":null,"comments":"Buy 89  Smartshares - NZ Mid Cap ETF (MDZ) at 5.504494","portfolio_id":123456,"holding_id":56156,"state":"confirmed","transaction_type":"BUY","instrument_id":1739485,"symbol":"MDZ","market":"NZX","attachment_filename":null,"attachment_id":null,"confirmed":true,"links":{"portfolio":"https://api.sharesight.com/api/v2/portfolios/123456"}},{"id":70041,"unique_identifier":"InvestNow:2022-02-23:477.27:133597:Buy 50  Smartshares - Australian Mid Cap ETF (MZY) at 9.5454","transaction_date":"2022-02-23","quantity":50.0,"price":9.5454,"cost_base":null,"exchange_rate":1.0,"brokerage":0.0,"brokerage_currency_code":"NZD","value":477.27,"paid_on":null,"company_event_id":null,"comments":"Buy 50  Smartshares - Australian Mid Cap ETF (MZY) at 9.5454","portfolio_id":123456,"holding_id":62100,"state":"confirmed","transaction_type":"BUY","instrument_id":9486,"symbol":"MZY","market":"NZX","attachment_filename":null,"attachment_id":null,"confirmed":true,"links":{"portfolio":"https://api.sharesight.com/api/v2/portfolios/123456"}},{"id":70039,"unique_identifier":"InvestNow:2022-02-23:495.94:143749:Buy 349  Smartshares - Australian Property ETF (ASP) at 1.421032","transaction_date":"2022-02-23","quantity":349.0,"price":1.421032,"cost_base":null,"exchange_rate":1.0,"brokerage":0.0,"brokerage_currency_code":"NZD","value":495.94,"paid_on":null,"company_event_id":null,"comments":"Buy 349  Smartshares - Australian Property ETF (ASP) at 1.421032","portfolio_id":123456,"holding_id":62104,"state":"confirmed","transaction_type":"BUY","instrument_id":3842,"symbol":"ASP","market":"NZX","attachment_filename":null,"attachment_id":null,"confirmed":true,"links":{"portfolio":"https://api.sharesight.com/api/v2/portfolios/123456"}},{"id":71270042,"unique_identifier":"InvestNow:2022-02-23:496.23:137500:Buy 430  Smartshares - NZ Property ETF (NPF) at 1.154023","transaction_date":"2022-02-23","quantity":430.0,"price":1.154023,"cost_base":null,"exchange_rate":1.0,"brokerage":0.0,"brokerage_currency_code":"NZD","value":496.23,"paid_on":null,"company_event_id":null,"comments":"Buy 430  Smartshares - NZ Property ETF (NPF) at 1.154023","portfolio_id":123456,"holding_id":27243,"state":"confirmed","transaction_type":"BUY","instrument_id":7461,"symbol":"NPF","market":"NZX","attachment_filename":null,"attachment_id":null,"confirmed":true,"links":{"portfolio":"https://api.sharesight.com/api/v2/portfolios/123456"}},{"id":70037,"unique_identifier":"InvestNow:2022-02-23:1000:153489:Buy 888.9679  Te Ahumairangi Global Equity Fund at 1.1249","transaction_date":"2022-02-23","quantity":888.9679,"price":1.1249,"cost_base":null,"exchange_rate":1.0,"brokerage":0.0,"brokerage_currency_code":"NZD","value":1000.0,"paid_on":null,"company_event_id":null,"comments":"Buy 888.9679  Te Ahumairangi Global Equity Fund at 1.1249","portfolio_id":123456,"holding_id":95375,"state":"confirmed","transaction_type":"BUY","instrument_id":3225,"symbol":"25134","market":"FundNZ","attachment_filename":null,"attachment_id":null,"confirmed":true,"links":{"portfolio":"https://api.sharesight.com/api/v2/portfolios/123456"}},{"id":69861,"unique_identifier":"InvestNow:2022-02-23:1000:151532:Buy 991.6415  Squirrel Monthly Income Fund at 1.0084","transaction_date":"2022-02-23","quantity":991.6415,"price":1.0084,"cost_base":null,"exchange_rate":1.0,"brokerage":0.0,"brokerage_currency_code":"NZD","value":999.97,"paid_on":null,"company_event_id":null,"comments":"Buy 991.6415  Squirrel Monthly Income Fund at 1.0084","portfolio_id":123456,"holding_id":3278,"state":"confirmed","transaction_type":"BUY","instrument_id":4423,"symbol":"25122","market":"FundNZ","attachment_filename":null,"attachment_id":null,"confirmed":true,"links":{"portfolio":"https://api.sharesight.com/api/v2/portfolios/123456"}},{"id":80038,"unique_identifier":"InvestNow:2022-02-23:995:164898:Buy 958.2234  Foundation Series Total World Fund at 1.0384","transaction_date":"2022-02-23","quantity":958.2234,"price":1.0384,"cost_base":null,"exchange_rate":1.0,"brokerage":0.0,"brokerage_currency_code":"NZD","value":995.02,"paid_on":null,"company_event_id":null,"comments":"Buy 958.2234  Foundation Series Total World Fund at 1.0384","portfolio_id":123456,"holding_id":3690,"state":"confirmed","transaction_type":"BUY","instrument_id":1561,"symbol":"25426","market":"FundNZ","attachment_filename":null,"attachment_id":null,"confirmed":true,"links":{"portfolio":"https://api.sharesight.com/api/v2/portfolios/123456"}},{"id":59067,"unique_identifier":null,"transaction_date":"2022-11-28","quantity":3620.8027,"price":1.4768,"cost_base":null,"exchange_rate":0.92765,"brokerage":0.0,"brokerage_currency_code":"AUD","value":-5347.20,"paid_on":null,"company_event_id":null,"comments":"Sell 3620.8027  Vanguard International Shares Select Exclusions Index Fund at 1.4768 (Ex rate 0.9277 approx)","portfolio_id":213412,"holding_id":56210,"state":"confirmed","transaction_type":"SELL","instrument_id":1742770,"symbol":"VAN1579AU","market":"FundAU","attachment_filename":"investmenttransactions-58428-2022-12-05-to-2022-12-31-MONTH-sharesight.csv","attachment_id":9100,"confirmed":true,"links":{"portfolio":"https://api.sharesight.com/api/v2/portfolios/213412"}}],"api_transaction":{"id":76496,"version":2,"action":"/api/v2/portfolios/123456/trades","timestamp":"2022-05-01T12:35:03.000+02:00"},"links":{"self":"https://api.sharesight.com/api/v2/portfolios/123456/trades?end_date=2022-02-23\u0026start_date=2022-02-23"}}"""
trades_empty_reponse = """{"trades":[],"api_transaction":{"id":562130018,"version":2,"action":"/api/v2/portfolios/123456/trades","timestamp":"2022-05-02T16:00:07.000+10:00"},"links":{"self":"https://api.sharesight.com/api/v2/portfolios/123456/trades?end_date=2023-02-24\u0026start_date=2023-02-24"}}"""

@testset "Get Sharesight transactions from API response" begin
    trades_df = Sharesight.get_trades_from_api_response(trades_response)
    @test !isnothing(trades_df)
    @test nrow(trades_df) == 8
    @test names(trades_df) == ["transaction_date", "transaction_type", "market", "symbol", "quantity", "price", "portfolio_id", "instrument_id", "state", "brokerage", "id", "value", "comments", "holding_id", "exchange_rate", "unique_identifier", "transaction"]

    trades_empty_df = Sharesight.get_trades_from_api_response(trades_empty_reponse)
    @test !isnothing(trades_empty_df)
    @test nrow(trades_empty_df) == 0
    @test names(trades_empty_df) == ["transaction_date", "transaction_type", "market", "symbol", "quantity", "price", "portfolio_id", "instrument_id", "state", "brokerage", "id", "value", "comments", "holding_id", "exchange_rate", "unique_identifier", "transaction"]
end