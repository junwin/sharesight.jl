module Sharesight

export SharesightTransaction
export SharesightClientCredentials
export OAuthInfo

using Dates
using HTTP
using JSON
using JSONTables
using DataFrames
using CSV

struct SharesightTransaction
    tradedate::Date
    transactiontype::Union{String, Nothing}
    marketcode::Union{String, Nothing}
    instrumentcode::Union{String, Nothing}
    comment::Union{String, Nothing}
    quantity::Union{Real, Nothing}
    price::Union{Real, Nothing}
    amount::Union{Real, Nothing}
    currency::Union{String, Nothing}
    uniqueId::Union{String, Nothing}
    SharesightTransaction(
        tradedate,
        transactiontype,
        marketcode,
        instrumentcode,
        comment,
        quantity,
        price,
        amount,
        currency=nothing,
        uniqueId=nothing) = new(
            tradedate,
            isnothing(transactiontype) ? nothing : uppercase(transactiontype),
            marketcode,
            instrumentcode,
            comment,
            quantity,
            price,
            amount,
            currency,
            uniqueId)
end

function Base.isless(a::SharesightTransaction, b::SharesightTransaction)
    if a.tradedate == b.tradedate
        if a.marketcode == b.marketcode
            if a.instrumentcode == b.instrumentcode
                if a.transactiontype == b.transactiontype
                    if a.quantity == b.quantity
                        return a.price < b.price
                    else
                        return a.quantity < b.quantity
                    end
                else
                    return a.transactiontype < b.transactiontype
                end
            else
                return a.instrumentcode < b.instrumentcode
            end
        else
            return a.marketcode < b.marketcode
        end
    else
        return a.tradedate < b.tradedate
    end
end

function Base.isequal(a::SharesightTransaction, b::SharesightTransaction)
    if !isnothing(a.uniqueId) && !isnothing(b.uniqueId) 
        if isequal(a.uniqueId, b.uniqueId)
            #println("Overriding normal equality check, using uniqueId")
            return true
        else
            return false
        end
    end

    if a.tradedate == b.tradedate && 
        a.marketcode == b.marketcode &&
        a.instrumentcode == b.instrumentcode && 
        a.transactiontype == b.transactiontype && 
        isequal(a.quantity, b.quantity) &&
        isequal(a.price, b.price)
        return true
    end
    return false

        #isapprox(a.amount, b.amount; atol=0.01) &&  # Allow up to 0.01 difference to be considered equal

end

function whatisdifferent(a::SharesightTransaction, b::SharesightTransaction)
    if isequal(a, b)
        println("Nothing is different, they are equal")
    end
    if !isnothing(a.uniqueId) && !isnothing(b.uniqueId) && isequal(a.uniqueId, b.uniqueId)
        println("equal uniqueId")
    end
    if !isequal(a.uniqueId, b.uniqueId)
        println("Different uniqueId") 
    end
    if !isequal(a.tradedate, b.tradedate)
        println("Different tradedate") 
    end
    if !isequal(a.instrumentcode, b.instrumentcode)
        println("Different instrumentcode") 
    end
    if !isequal(a.transactiontype, b.transactiontype)
        println("Different transactiontype") 
    end
    if !isequal(a.quantity, b.quantity)
        println("Different quantity") 
    end
    if !isequal(a.price, b.price)
        println("Different price") 
    end
    if !isequal(a.amount, b.amount)
        println("Different amount (but that should be ignored for equality)") 
    end
end

# Need hash overridden because the antijoin seems to use hash to match left and right
function Base.hash(a::SharesightTransaction, h::UInt)
    #==
    hash_of_attrribs = hash(
            (hash(a.tradedate),
            (hash(a.marketcode),
            (hash(a.instrumentcode),
            (hash(a.transactiontype),
            (hash(a.amount),
            (hash(a.quantity),
            (hash(a.price), h))))))))
    ==#
    hash_of_attrribs = hash(
            (hash(a.tradedate),
            (hash(a.marketcode),
            (hash(a.instrumentcode),
            (hash(a.transactiontype),
            (hash(a.quantity),
            (hash(a.price))))))))

end

function Base.:(==)(a::SharesightTransaction, b::SharesightTransaction)
    return isequal(a, b)
end

function buysellfilter(t::SharesightTransaction)::Bool
    isnothing(t.transactiontype) ? false : t.transactiontype == "BUY" || t.transactiontype == "SELL"
end

#AUTHORIZE_TOKEN_URI = "https://api.sharesight.com/oauth2/authorize"
ACCESS_TOKEN_URI = "https://api.sharesight.com/oauth2/token"
V3_API_BASE_URI = "https://api.sharesight.com/api/v3"
V2_API_BASE_URI = "https://api.sharesight.com/api/v2"
V2_API_TAIL = ".json"

# Sharesight credentials for simple API use (using the client_credentials grant_type)
struct SharesightClientCredentials
    client_id::AbstractString
    client_secret::AbstractString
    #redirect_url::AbstractString
end

struct OAuthInfo
    access_token::String
    # refresh_token::String  # not used for client_credentials grant_type
    expires_in::Int
    expires_at::DateTime
    # previous::Union{OAuthInfo, Nothing}
    # status::Union{Int, Nothing}
end

function refresh_oauth(oauthinfo::Union{OAuthInfo, Nothing}, credentials::SharesightClientCredentials)
    if !isnothing(oauthinfo) && oauthinfo.expires_at > Dates.now()
        return oauthinfo
    else
        # Log in again
        bodytext = "grant_type=client_credentials&client_id=$(credentials.client_id)&client_secret=$(credentials.client_secret)"
        uri = "$(ACCESS_TOKEN_URI)?response_type=code&client_id=$(credentials.client_id)" #&redirect_uri=$(redirect_url)"

        @info "Requesting new OAuth permission..."
        resp = HTTP.post(uri; body=bodytext)
        @info "Success! ($(resp.status))"

        jsonbody = JSON.parse(String(resp.body))

        @info "Response body: $(jsonbody)"

        # newoauthinfo = OAuthInfo(jsonbody["access_token"], oauthinfo.refresh_token, jsonbody["expires_in"], Dates.now() + Dates.Second(details["expires_in"]))
        return OAuthInfo(jsonbody["access_token"], jsonbody["expires_in"], Dates.now() + Dates.Second(jsonbody["expires_in"]))
    end
        
end

function get_uri(version, api; parameters=nothing)
    if isnothing(parameters)
        parameters = ""
    elseif !startswith(parameters, "?")
        parameters = "?" * parameters
    end

    if (version == "V3")
        #uri = parameters === nothing ? "$V3_API_BASE_URI/$api" : "$V3_API_BASE_URI/$(api)?$(parameters)"
        uri = "$V3_API_BASE_URI/$(api)$(parameters)"
    else
        # uri = "$V2_API_BASE_URI/$(api)$(V2_API_TAIL)"  # Don't need the tail?
        #uri = parameters === nothing ? "$V2_API_BASE_URI/$(api)" : "$V2_API_BASE_URI/$(api)?$(parameters)"
        uri = "$V2_API_BASE_URI/$(api)$(parameters)"
    end
    return uri
end

function get_api(oauthinfo::Union{OAuthInfo, Nothing}, credentials::SharesightClientCredentials, api::AbstractString; parameters="", version="V3")
    # Ensure access_token is current
    oauthinfo = refresh_oauth(oauthinfo, credentials)

    headers = Dict("Authorization" => "Bearer " * oauthinfo.access_token)
    uri = get_uri(version, api; parameters)

    @info "Requesting $api"
    @info "URI: $uri"
    @info "PARAMETERS: $parameters"
    resp = HTTP.get(uri; headers)

    return (oauthinfo, String(resp.body))

end

function get_portfolios(oauthinfo::Union{OAuthInfo, Nothing}, credentials::SharesightClientCredentials)
    oauthinfo, portfolios_string = get_api(oauthinfo, credentials, "portfolios")
        
    portfolios_json = JSON.parse(portfolios_string)["portfolios"]
    
    return (oauthinfo, portfolios_json)
end

function get_portfolio_holdings(oauthinfo::Union{OAuthInfo, Nothing}, credentials::SharesightClientCredentials, portfolioid)
    oauthinfo, resp_string = get_api(oauthinfo, credentials, "portfolios/$(portfolioid)/holdings")
        
    portfolio_holdings_json = JSON.parse(resp_string)["holdings"]
    
    return (oauthinfo, portfolio_holdings_json)
end

function get_trades_from_api_response(portfolios_string::String)

    portfolios_json = JSON.parse(portfolios_string)["trades"]

    trades_df = DataFrame(portfolios_json)

    if nrow(trades_df) > 0
        select!(trades_df, :transaction_date => ByRow(d -> Date(d, dateformat"y-m-d")) => :transaction_date, :transaction_type, :market, :symbol, :quantity, :price, :portfolio_id, :instrument_id, :state, :brokerage, :id, :value, :comments, :holding_id, :exchange_rate, :unique_identifier)

        trades_df.transaction .= SharesightTransaction.(trades_df.transaction_date, trades_df.transaction_type, trades_df.market, trades_df.symbol, trades_df.comments, trades_df.quantity, trades_df.price, trades_df.value, nothing, trades_df.unique_identifier)
    else
        # Create an empty DataFrame of the correct definition
        return DataFrame(transaction_date = Date[], 
            transaction_type = String[], 
            market = String[], 
            symbol = String[],
            quantity = Float64[],
            price = Float64[],
            portfolio_id = Int64[],
            instrument_id = Int64[],
            state = String[],
            brokerage = Float64[],
            id = Int64[],
            value = Float64[],
            comments = String[],
            holding_id = Int64[],
            exchange_rate = Float64[],
            unique_identifier = String[],
            transaction = SharesightTransaction[])
    end

    return trades_df
end

function get_trades_json(oauthinfo::Union{OAuthInfo, Nothing}, credentials::SharesightClientCredentials, portfolioid::Int; start_date::Date=nothing, end_date::Date=nothing)

    parameters = "?start_date=$(Dates.format(start_date, "yyyy-mm-dd"))&end_date=$(Dates.format(end_date, "yyyy-mm-dd"))"
    oauthinfo, portfolios_string = get_api(oauthinfo, credentials, "portfolios/$(portfolioid)/trades"; parameters, version="V2")
        
    portfolios_json = JSON.parse(portfolios_string)["trades"]

    return (oauthinfo, portfolios_json)
end

function get_trades_df(oauthinfo::Union{OAuthInfo, Nothing}, credentials::SharesightClientCredentials, portfolioid::Int; start_date::Date=nothing, end_date::Date=nothing)

    parameters = "?start_date=$(Dates.format(start_date, "yyyy-mm-dd"))&end_date=$(Dates.format(end_date, "yyyy-mm-dd"))"
    oauthinfo, portfolios_string = get_api(oauthinfo, credentials, "portfolios/$(portfolioid)/trades"; parameters, version="V2")

    return (oauthinfo, get_trades_from_api_response(portfolios_string))
end

"Get trades for a specific portfolio in a specific date range"
function get_trades_df(client_id, client_secret, start_date, end_date, portfolioName)
    credentials = SharesightClientCredentials(client_id, client_secret)

    oauthinfo = Sharesight.refresh_oauth(nothing, credentials)

    oauthinfo, ss_portfolios = Sharesight.get_portfolios(oauthinfo, credentials)

    @info "Portfolios: $ss_portfolios"

    # filter by name, return the id
    portfolioid = filter(p -> p["name"] == portfolioName, ss_portfolios)[1]["id"]

    @info "Portfolio id: $portfolioid"
    
    return get_trades_df(oauthinfo, credentials, portfolioid; start_date=start_date, end_date=end_date)[2]
end

function post_api(oauthinfo::Union{OAuthInfo, Nothing}, credentials::SharesightClientCredentials, api::AbstractString, body; version="V3")
    # Ensure access_token is current
    oauthinfo = refresh_oauth(oauthinfo, credentials)

    headers = Dict("Authorization" => "Bearer " * oauthinfo.access_token, "Content-Type" => "application/json")
    uri = get_uri(version, api)

    @info "Posting $api"
    @info "URI: $uri"

    try
        resp = HTTP.post(uri, headers, body)
        return (oauthinfo, String(resp.body))
    
    catch e
        if e.status != 422
            @warn "Something has gone wrong! HTTP $(e.status)"
            throw(e)
        else
            # OK
            @info "HTTP 422"
            resp_body = String(e.response.body)
            @info "resp_body: $resp_body"

            return (oauthinfo, resp_body)
        end

    end

    return (oauthinfo, String(resp.body))

end

function create_trade(oauthinfo::Union{OAuthInfo, Nothing}, credentials::SharesightClientCredentials, portfolioid::Int, trade::SharesightTransaction)

    return create_trade(oauthinfo, credentials, portfolioid, trade.tradedate, trade.transactiontype, trade.marketcode, trade.instrumentcode, trade.comment, trade.quantity, trade.price, trade.amount, trade.currency, trade.uniqueId)

end

function create_trade(client_id, client_secret, portfolioName, trade)

    credentials = SharesightClientCredentials(client_id, client_secret)

    oauthinfo = Sharesight.refresh_oauth(nothing, credentials)

    oauthinfo, ss_portfolios = Sharesight.get_portfolios(oauthinfo, credentials)

    @info "Portfolios: $ss_portfolios"

    # filter by name, return the id
    portfolioid = filter(p -> p["name"] == portfolioName, ss_portfolios)[1]["id"]

    @info "Portfolio id: $portfolioid"
    
    return create_trade(oauthinfo, credentials, portfolioid, trade.tradedate, trade.transactiontype, trade.marketcode, trade.instrumentcode, trade.comment, trade.quantity, trade.price, trade.amount, trade.currency, trade.uniqueId)

end

function create_trade(oauthinfo::Union{OAuthInfo, Nothing}, credentials::SharesightClientCredentials, portfolioid::Int, tradedate, transactiontype, marketcode, instrumentcode, comment, quantity, price, amount, currency, uniqueId)

    trade_dict = Dict("transaction_date" => Dates.format(tradedate, "yyyy-mm-dd"),
        "portfolio_id" => portfolioid,
        "market" => marketcode,
        "symbol" => instrumentcode,
        "transaction_type" => transactiontype,
        "quantity" => quantity,
        "price" => price,
        "state" => "unconfirmed",
        "unique_identifier" => uniqueId,
        "comments" => comment
    )

    trade_body = Dict("trade" => trade_dict)
    trade_body_json = JSON.json(trade_body)
    @info "trade_body_json: $trade_body_json"

    oauthinfo, resp_body = Sharesight.post_api(oauthinfo, credentials, "trades.json", trade_body_json; version="V2")

    return(oauthinfo, resp_body)
end

function writetxncsv(txns::Vector{SharesightTransaction}, outfile::String)
    CSV.write(outfile, txns; transform=(col, val) -> something(val, missing), header=["Trade Date", "Transaction Type", "Market Code", "Instrument Code", "Comments", "Quantity", "Price", "Amount", "Currency", "Unique Id"])
end

end # module Sharesight