### A Pluto.jl notebook ###
# v0.19.22

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ 1a8c19e8-97a2-4b66-89fe-48f776590c2f
begin
    import Pkg
    Pkg.activate(Base.current_project())
	Pkg.instantiate()
    using Revise, Sharesight, HTTP, JSON, JSONTables, DataFrames, Dates, PlutoUI, CSV
end

# ╔═╡ cbe3f8c5-d40b-4bb0-ab4e-9ebbb8b6fc3b
# Load secrets
begin
	include("../config/secrets.jl")
	credentials = SharesightClientCredentials(client_id, client_secret)
end

# ╔═╡ c8b44d03-28ec-44ce-8567-bc1fb91b698d
md"""
# Create trades on Sharesight for new InvestNow trades
"""

# ╔═╡ ed212393-e0aa-4132-ab4d-cadaaaed5411
md"""
Run the Pluto notebook using the command:

```sh
$ julia --project -e 'using Pluto; Pluto.run();'
```
"""

# ╔═╡ 52e3c8db-1d92-4d59-9019-130980ec85ea
md"""
Pull in Investnow transactions...
"""

# ╔═╡ 0eb18268-765f-4d8e-bbaf-1f4994ea2e08
investnow_transactions = CSV.read("../../../InvestNow/investnow.jl/output/investmenttransactions-58428-2023-02-26-to-2023-02-28-YEAR-sharesight.csv", DataFrame)

# ╔═╡ 70ccf464-397f-4bed-a1f6-98a017e493fb
names(investnow_transactions)

# ╔═╡ e00fab16-a70c-4f4c-9b67-3014a4ec3fe4
investnow_code_gdf = groupby(investnow_transactions, ["Market Code", "Instrument Code"])

# ╔═╡ 5974664b-c9b6-4675-bb16-9eafd7b428bc
investnow_code_gdf[2]

# ╔═╡ d32f2dd4-1953-4a53-b316-e70728b9a6f3
combine(investnow_code_gdf, nrow, "Trade Date" => minimum, "Trade Date" => maximum)

# ╔═╡ 092890a7-5606-472f-bc9b-157e7ccdac25
begin
	summary_df = combine(investnow_transactions, nrow, "Trade Date" => minimum, "Trade Date" => maximum)
	start_date = summary_df[!, 2][1]
	end_date = summary_df[!, 3][1]
	display("$start_date to $end_date")
end

# ╔═╡ 2d52532a-2214-4c0f-9c47-a03b7892ad04
investnow_transactions[!, "Amount"]

# ╔═╡ 718046a6-d74b-41f3-8b3c-e5bb4090c90f
begin
	oauthinfo = Sharesight.refresh_oauth(nothing, credentials)
	display(oauthinfo)
end

# ╔═╡ c73ecf70-90f6-4ac5-916f-b24fbe37b826

# Get portfolios
begin
	oauthinfo1, portfolios = Sharesight.get_portfolios(oauthinfo, credentials)
	display("$(length(portfolios)) portfolios")
	#display(keys(portfolios[1]))
	for p in portfolios
		display("$(p["name"]), id:$(p["id"])")
	end
	#display(oauthinfo1)
end

# ╔═╡ 0e7e10e2-4926-4740-bda0-774231a98d6c
begin
	portfolio_ids = [ i => portfolios[i]["name"] for i = 1:length(portfolios)]
	display(portfolio_ids)
end

# ╔═╡ f4222170-07d5-4e61-8f72-e7a5613c9966
@bind selected_portfolio Select(portfolio_ids)

# ╔═╡ 190c90ec-2c28-4247-ba35-bf613c6b52fe
# Get portfolio holdings
begin
	display("$(portfolios[selected_portfolio]["id"]): $(portfolios[selected_portfolio]["name"])")
	oauthinfo3, holdings = Sharesight.get_portfolio_holdings(oauthinfo, credentials, portfolios[selected_portfolio]["id"])
	display("$(length(holdings)) holdings")
	#display(holdings)
	for h in holdings
		display("$(h["instrument"]["code"]) - $(h["instrument"]["name"])")
	end
end

# ╔═╡ b8419312-964e-434e-97c9-6acffcb87d04


# ╔═╡ 640bff1f-6de0-4c7e-a2b2-120d4c13701f
begin
	uri = "/portfolios/$(portfolios[selected_portfolio]["id"])/holdings"
	oauthinfo2, respbody = Sharesight.get_api(oauthinfo, credentials, uri)
	display(respbody)
end

# ╔═╡ cd1270e3-ce67-4be9-9e65-c49c5d9700a4
begin
	today5 = Dates.today()
	#end_date5 = today5
	#start_date5 = end_date5 - Dates.Year(1) + Dates.Day(0)
	display("from $(start_date) to $(end_date)")
	display("portfolio $(portfolios[selected_portfolio]["id"])")

	#uri4 = "/portfolios/$(portfolios[selected_portfolio]["id"])/trades"
	#parameters4 = "?start_date=$(start_date)&end_date=$(end_date)"

	#display(uri4)
	#display(parameters4)
	
	oauthinfo5, trades = Sharesight.get_trades(oauthinfo, credentials, portfolios[selected_portfolio]["id"]; start_date=start_date, end_date=end_date)
	display(trades)
end

# ╔═╡ 2e76039d-f9b6-421d-a03d-b68af01eea6d
trades

# ╔═╡ 87079ed0-9537-4892-be7e-bfb95a830e8c
trades_df = DataFrame(trades)

# ╔═╡ 6cdcfe65-df9b-4132-bf1f-de517f83ae69
size(trades_df)[1]

# ╔═╡ 106deda9-3600-4e32-8313-2a31c4670c2d
names(trades_df)

# ╔═╡ 46f9a21f-88c9-4dbf-81cb-28f99438be84
md"Drop unnneded columns and convert the date to a Date"

# ╔═╡ c36cc240-328a-47ec-8bde-abb853a0c2d5
select!(trades_df, :transaction_date => ByRow(d -> Date(d, dateformat"y-m-d")) => :transaction_date, :transaction_type, :market, :symbol, :quantity, :price, :portfolio_id, :instrument_id, :state, :brokerage, :id, :value, :comments, :holding_id, :exchange_rate)

# ╔═╡ 843c250d-db00-4980-b990-e1409a310b1a
sharesight_trades_gdf = groupby(trades_df, [:market, :symbol])

# ╔═╡ b251f10b-f481-49c4-a6d3-d0d1fa24d31d
md"Transactions from Sharesight that have not been found in Investnow transactions:"

# ╔═╡ d5917355-9434-435a-91ee-8be36962f460
antijoin(trades_df, investnow_transactions, on=["transaction_date" => "Trade Date", "market" => "Market Code", "symbol" => "Instrument Code"])

# ╔═╡ 787932bb-51e3-4baf-9ff7-e25347f7dd30
md"Transactions from Investnow that have not been recorded in Sharesight:"

# ╔═╡ 5060da86-5d98-41f6-a271-68d080b6b7e7
unrecorded_trades_df_all = antijoin(investnow_transactions, trades_df, on=["Trade Date" => "transaction_date", "Market Code" => "market", "Instrument Code" => "symbol"])

# ╔═╡ 456b0c54-a393-4d74-aed0-def1b4718666
begin
	unrecorded_trades_df = first(unrecorded_trades_df_all, 1)
	display(unrecorded_trades_df)
end

# ╔═╡ c49de7fb-6e15-4c97-a7de-d1bb852995d9
begin
	display(unrecorded_trades_df[1, "Trade Date"])
	trade_dict = Dict("transaction_date" => Dates.format(unrecorded_trades_df[1, "Trade Date"], "yyyy-mm-dd"),
		"portfolio_id" => portfolios[selected_portfolio]["id"],
		"market" => unrecorded_trades_df[1, "Market Code"],
		"symbol" => unrecorded_trades_df[1, "Instrument Code"],
		"transaction_type" => unrecorded_trades_df[1, "Transaction Type"],
		"quantity" => unrecorded_trades_df[1, "Quantity"],
		"price" => unrecorded_trades_df[1, "Price"],
		"state" => "unconfirmed",
		"comments" => unrecorded_trades_df[1, "Comments"]
	)
	display(trade_dict)
	trade_body = Dict("trade" => trade_dict)
	display(trade_body)
	trade_body_json = JSON.json(trade_body)
	display(trade_body_json)
end

# ╔═╡ 2b1f2ef9-176d-4654-85da-f78064a07665
begin
	oauthinfo6, result6 = Sharesight.post_api(oauthinfo, credentials, "trades", trade_body_json, version="V2")
	display(result6)
end

# ╔═╡ Cell order:
# ╟─c8b44d03-28ec-44ce-8567-bc1fb91b698d
# ╟─ed212393-e0aa-4132-ab4d-cadaaaed5411
# ╠═1a8c19e8-97a2-4b66-89fe-48f776590c2f
# ╠═52e3c8db-1d92-4d59-9019-130980ec85ea
# ╠═0eb18268-765f-4d8e-bbaf-1f4994ea2e08
# ╠═70ccf464-397f-4bed-a1f6-98a017e493fb
# ╠═e00fab16-a70c-4f4c-9b67-3014a4ec3fe4
# ╠═5974664b-c9b6-4675-bb16-9eafd7b428bc
# ╠═d32f2dd4-1953-4a53-b316-e70728b9a6f3
# ╠═092890a7-5606-472f-bc9b-157e7ccdac25
# ╠═2d52532a-2214-4c0f-9c47-a03b7892ad04
# ╠═cbe3f8c5-d40b-4bb0-ab4e-9ebbb8b6fc3b
# ╟─718046a6-d74b-41f3-8b3c-e5bb4090c90f
# ╠═c73ecf70-90f6-4ac5-916f-b24fbe37b826
# ╠═0e7e10e2-4926-4740-bda0-774231a98d6c
# ╠═f4222170-07d5-4e61-8f72-e7a5613c9966
# ╠═190c90ec-2c28-4247-ba35-bf613c6b52fe
# ╠═b8419312-964e-434e-97c9-6acffcb87d04
# ╠═640bff1f-6de0-4c7e-a2b2-120d4c13701f
# ╠═cd1270e3-ce67-4be9-9e65-c49c5d9700a4
# ╠═2e76039d-f9b6-421d-a03d-b68af01eea6d
# ╠═87079ed0-9537-4892-be7e-bfb95a830e8c
# ╠═6cdcfe65-df9b-4132-bf1f-de517f83ae69
# ╠═106deda9-3600-4e32-8313-2a31c4670c2d
# ╟─46f9a21f-88c9-4dbf-81cb-28f99438be84
# ╠═c36cc240-328a-47ec-8bde-abb853a0c2d5
# ╠═843c250d-db00-4980-b990-e1409a310b1a
# ╟─b251f10b-f481-49c4-a6d3-d0d1fa24d31d
# ╠═d5917355-9434-435a-91ee-8be36962f460
# ╟─787932bb-51e3-4baf-9ff7-e25347f7dd30
# ╠═5060da86-5d98-41f6-a271-68d080b6b7e7
# ╠═456b0c54-a393-4d74-aed0-def1b4718666
# ╠═c49de7fb-6e15-4c97-a7de-d1bb852995d9
# ╠═2b1f2ef9-176d-4654-85da-f78064a07665
